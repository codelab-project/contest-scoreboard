/**
 * Required modules.
 */
const Score      = require('../models/score');
const Submission = require('../models/submission');
const {
    findGistById,
    createGist,
    createGistComment,
    getGistComments
} = require('../helpers/gists');


/**
 * Exports the Gist Controller.
 */
module.exports = {
    /**
     * Create a gist.
     *
     * @param {Object} Object   Model instance.
     * @return                  A gist object with its respective properties.
     */
    create: async (req, res, next) => {
        try {
            const { score } = req.value.body;

            // Get the submission object sent by the middleware (req.submission).
            const submission = req.submission;

            // Sanitize submission in order to push it as a comment into the gist.
            const sanitized = `${submission.contestantNumber} ${submission.solutions} ${submission.timeSpent} ${submission.submissionType}`;

            // Check if user has an existing score.
            const existingScore = await Score.findById(score).populate('submissions');

            // Finish the process if no score was found.
            if (!existingScore) {
                return res.status(404).json({
                    success: false,
                    message: `No score was found with id '${score}'.`
                });
            }

            const submissions = existingScore.submissions.map(submission => `${submission.contestantNumber} ${submission.solutions} ${submission.timeSpent} ${submission.submissionType}`);

            // Dynamic objects.
            const gist = {};

            let gistSubmitted = {};

            // If the score has no a gist assigned yet.
            if (!existingScore.gistId) {
                // Push the gist to github.
                for (let i = 0; i < submissions.length; i++) {
                    const submission = submissions[i];

                    // Create the gist with the first submission as value.
                    if (i === 0) {
                        gistSubmitted = await createGist(existingScore._id, submission);
                    } else {
                        // Add all the remaining values (within the stored submissions) as comments.
                        await createGistComment(gistSubmitted.id, submission);
                    }
                }

                // Add the new comment coming within the request body.
                await createGistComment(gistSubmitted.id, sanitized);
            } else {
                // Add the new comment to the gist.
                await createGistComment(existingScore.gistId, sanitized);

                // Fetch the updated gits!
                gistSubmitted = await findGistById(existingScore.gistId);
            }

            // Create the submission document passed by the middleware.
            const submissionDocument = await Submission.create(submission);

            // Update the existingScore document adding the missing gistId and the new submission.
            await Score.findByIdAndUpdate(existingScore._id, {
                $set: {
                    gistId: gistSubmitted.id
                },
                $push: {
                    submissions: submissionDocument
                }
            }, {
                new: true
            });

            gist.id          = gistSubmitted.id;
            gist.url         = gistSubmitted.url;
            gist.comments    = gistSubmitted.comments;
            gist.owner       = {};
            gist.owner.id    = gistSubmitted.owner.id;
            gist.owner.login = gistSubmitted.owner.login;

            // Return the new score.
            return res.status(201).json(gist);
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Update a gist via PUT method.
     *
     * @param {String} id field           Gist ID.
     * @param {Object} JSON body object   A JSON object with all the required properties to update a gist via PUT method.
     * @return                            The updated gist object with its respective properties.
     */
    createGistComment: async (req, res, next) => {
        try {
            // Custom value created by the validator.
            const { id } = req.value.params;

            // Get the submission object sent by the middleware (req.submission).
            const submission = req.submission;

            // Sanitize submission in order to push it as a comment into the gist.
            const sanitized = `${submission.contestantNumber} ${submission.solutions} ${submission.timeSpent} ${submission.submissionType}`;

            // Check if user has an existing score.
            const existingScore = await Score.findById(id);

            // Finish the process if no score was found.
            if (!existingScore) {
                return res.status(404).json({
                    success: false,
                    message: `No score was found with id '${id}'.`
                });
            }

            // Fetch the gist in order to get some properties.
            const gist = await findGistById(existingScore.gistId);

            // Finish the process if no gist was found.
            if (!gist) {
                return res.status(404).json({
                    success: false,
                    message: `No gist was found for the score with id '${id}'.`
                });
            }

            // Create the submission document passed by the middleware.
            const submissionDocument = await Submission.create(submission);

            // Update the existingScore document adding the new submission.
            await Score.findByIdAndUpdate(existingScore._id, {
                $push: {
                    submissions: submissionDocument
                }
            }, {
                new: true
            });

            // Create a new comment to the existing gist.
            // Assume the gist is not missing, otherwise an additional validation will be required!
            const gistComment = await createGistComment(existingScore.gistId, sanitized);

            // Return the updated document.
            return res.status(200).json(gistComment);
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Read (retrieve) gist comments.
     *
     * @param {String} id field   Gist ID.
     * @return                    An array of object with their respective properties.
     */
    getGistComments: async (req, res, next) => {
        try {
            // Custom field 'value' created by joi validator.
            const { gistId } = req.value.params;

            // Fetch the gist comments.
            const comments = await getGistComments(gistId);

            // Finish the process if no scoreboard was found.
            if (!comments) {
                return res.status(404).json({
                    success: false,
                    message: `No gist comments found for the gist with id '${id}'.`
                });
            }

            return res.status(200).json(comments);
        } catch (error) {
            return next(error);
        }
    }
};
