/**
 * Required modules.
 */
const Score           = require('../models/score');
const Scoreboard      = require('../models/scoreboard');
const Submission      = require('../models/submission');
const { filterDocs }  = require('../helpers/filters');
const { includeDocs } = require('../helpers/inclusions');


/**
 * Exports the Scoreboards Controller.
 */
module.exports = {
    /**
     * Read (retrieve) scoreboards.
     *
     * @param {Object} filter field   Query strings object.
     * @return                        An array of filtered elements based on the query strings.
     */
    find: async (req, res, next) => {
        try {
            const { filter } = req.query;

            const inclusion = includeDocs(filter);

            const scoreboards = await Scoreboard.find().populate({
                path: `${inclusion.prop}`,
                select: `${inclusion.props}`
            }).exec();

            // Finish the process if no scoreboards were found.
            if (scoreboards.length === 0) {
                return res.status(404).json({
                    success: false,
                    message: 'No scoreboards were found.'
                });
            }

            const filteredScoreboards = filterDocs(scoreboards, filter);

            return res.status(200).json(filteredScoreboards);
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Read (retrieve) a single scoreboard.
     *
     * @param {String} id field   Scoreboard ID in MongoDB.
     * @return                    A scoreboard object with its respective properties.
     */
    findById: async (req, res, next) => {
        try {
            // Custom field 'value' created by joi validator.
            const { id } = req.value.params;

            const scoreboard = await Scoreboard.findById(id);

            // Finish the process if no scoreboard was found.
            if (!scoreboard) {
                return res.status(404).json({
                    success: false,
                    message: `No scoreboard was found with id '${id}'.`
                });
            }

            return res.status(200).json(scoreboard);
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Create a scoreboard.
     *
     * @param {Object} Object   Model instance.
     * @return                  A score object with its respective properties.
     */
    create: async (req, res, next) => {
        try {
            const { score } = req.value.body;

            const existingScore = await Score.findById(score);

            // Finish the process if no scoreboard was found.
            if (!existingScore) {
                return res.status(404).json({
                    success: false,
                    message: `No scoreboard was found with id '${score}'.`
                });
            }

            const submissions = await Submission.aggregate([
                {
                    $match: {
                        contestantNumber: existingScore.contestant,
                        submissionType: {
                            $in: ["C", "I"]
                        }
                    }
                },
                {
                    $group: {
                        _id: {
                            contestant: "$contestantNumber"
                        },
                        incorrect: {
                            $sum: {
                                $cond: [{ $eq: ["$submissionType", "I"]}, 20, 0]
                            }
                        },
                        correct: {
                            $sum: {
                                $cond: [{ $eq: ["$submissionType", "C"]}, "$timeSpent", 0]
                            }
                        },
                        count: {
                            $sum: 1
                        }
                    }
                }
            ]);

            const submission  = submissions[0];
            const calculation = submission.incorrect + submission.correct;

            const existingScoreboard = await Scoreboard.findOne({ score });

            let scoreboard = {};

            if (!existingScoreboard) {
                scoreboard = await Scoreboard.create({
                    calculation: calculation,
                    score: existingScore
                });
            } else {
                scoreboard = await Scoreboard.findByIdAndUpdate(existingScoreboard._id, {
                    calculation: calculation
                }, {
                    new: true
                });
            }

            // Return the scoreboard.
            return res.status(201).json(scoreboard);
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Delete a scoreboard.
     *
     * @param {String} id field   Scoreboard ID in MongoDB.
     * @return                    A JSON object with its respective details about the process.
     */
    destroyById: async (req, res, next) => {
        try {
            // Custom value created by the validator.
            const { id } = req.value.params;

            const scoreboard = await Scoreboard.findById(id);

            // Finish the process if no scoreboard was found.
            if (!scoreboard) {
                return res.status(404).json({
                    success: false,
                    message: `No scoreboard was found with id '${id}'. Perhaps it was already deleted!`
                });
            }

            // Delete the score in case of it has not been deleted yet.
            await Scoreboard.findByIdAndRemove(id);

            /**
             * 204 code may be appropriate if a document was deleted and it may be good for requests via ajax,
             * however the process was successfully performed and we need to inform it to the front.
             */
            return res.status(200).json({
                success: true,
                message: `The scoreboard with id '${id}' was successfully deleted!`
            });
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Delete multiple scoreboards.
     *
     * @param {Object} filter field   Query strings object.
     * @return                        A JSON object with its respective details about the process.
     */
    destroy: async (req, res, next) => {
        try {
            const { filter } = req.query;

            let props;
            const query = {};

            const inclusion = includeDocs(filter);

            // Find scoreboard docs matching the property coming through filter.
            const scoreboards = await Scoreboard.find({ [`${inclusion.prop}`]: { $exists: true } });

            // Finish the process if no scoreboards were found with the specified property.
            if (scoreboards.length === 0) {
                return res.status(404).json({
                    success: false,
                    message: `No scoreboards were found with the property '${inclusion.prop}'.`
                });
            }

            // Removes the unnecessary -_id property added by the method.
            if (inclusion.props.match(/ -_id/i)) {
                props = inclusion.props.replace(/ -_id/i, '');
            }

            // Allow only a single property in order to create a regex for an specific field.
            if (typeof inclusion.prop !== 'string') {
                return res.status(400).json({
                    success: false,
                    message: 'The include method only supports a single property.'
                });
            }

            // Validate the regex value in order to avoid deleting the entire scoreboards collection.
            if (!props) {
                return res.status(400).json({
                    success: false,
                    message: 'The scoreboards deletion process cannot be completed due to there is not a value for the regex.'
                });
            }

            // Construct the query dynamically.
            query[`${inclusion.prop}`] = {
                $regex: props,
                $options: 'i'
            };

            // Remove all the docs matching the query.
            await Scoreboard.remove(query);

            /**
             * 204 code may be appropriate if a document was deleted and it may be good for requests via ajax,
             * however the process was successfully performed and we need to inform it to the front.
             */
            return res.status(200).json({
                success: true,
                message: `All the scoreboards matching the regex '${props}' were successfully deleted!`
            });
        } catch (error) {
            return next(error);
        }
    }
};
