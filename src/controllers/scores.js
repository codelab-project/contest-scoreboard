/**
 * Required modules.
 */
const Score           = require('../models/score');
const Submission      = require('../models/submission');
const { filterDocs }  = require('../helpers/filters');
const { includeDocs } = require('../helpers/inclusions');


/**
 * Exports the Scores Controller.
 */
module.exports = {
    /**
     * Read (retrieve) scores.
     *
     * @param {Object} filter field   Query strings object.
     * @return                        An array of filtered elements based on the query strings.
     */
    find: async (req, res, next) => {
        try {
            const { filter } = req.query;

            const inclusion = includeDocs(filter);

            const scores = await Score.find().populate({
                path: `${inclusion.prop}`,
                select: `${inclusion.props}`
            }).exec();

            // Finish the process if no scores were found.
            if (scores.length === 0) {
                return res.status(404).json({
                    success: false,
                    message: 'No scores were found.'
                });
            }

            const filteredScores = filterDocs(scores, filter);

            return res.status(200).json(filteredScores);
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Read (retrieve) a single score.
     *
     * @param {String} id field   Score ID in MongoDB.
     * @return                    A score object with its respective properties.
     */
    findById: async (req, res, next) => {
        try {
            // Custom field 'value' created by joi validator.
            const { id } = req.value.params;

            const score = await Score.findById(id);

            // Finish the process if no score was found.
            if (!score) {
                return res.status(404).json({
                    success: false,
                    message: `No score was found with id '${id}'.`
                });
            }

            return res.status(200).json(score);
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Create score.
     *
     * @param {Object} Object   Model instance.
     * @return                  A score object with its respective properties.
     */
    create: async (req, res, next) => {
        try {
            // Get the submission object sent by the middleware (req.submission).
            const submission = req.submission;

            // Take a single submission.
            // Check if the contestant has an existing score.
            const existingScore = await Score.findOne({ contestant: submission.contestantNumber });

            // Create the submission document.
            const submissionDocument = await Submission.create(submission);

            let score = {};

            // If the score does not exist, create a new score.
            if (!existingScore) {
                // Create the score document.
                score = await Score.create({
                    contestant: submission.contestantNumber,
                    submissions: [submissionDocument]
                });
            } else {
                // Update the score document with the new submission.
                score = await Score.findOneAndUpdate({
                    _id: existingScore._id
                }, {
                    $push: {
                        submissions: submissionDocument
                    }
                }, {
                    new: true
                });
            }

            // Return the score.
            return res.status(201).json(score);
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Update a score via PUT method.
     *
     * @param {String} id field           Score ID in MongoDB.
     * @param {Object} JSON body object   A JSON object with all the required properties to update a score via PUT method.
     * @return                            The updated score object with its respective properties.
     */
    replaceById: async (req, res, next) => {
        try {
            // Custom value created by the validator.
            const { id } = req.value.params;
            // Enforce that the req.value.body must contain all the fields.
            const newScore = req.value.body;
            const updatedScore = await Score.findByIdAndUpdate(id, newScore, { new: true });

            // Finish the process if no score was found.
            if (!updatedScore) {
                return res.status(404).json({
                    success: false,
                    message: `No score was found with id '${id}'.`
                });
            }

            // Return the updated document.
            return res.status(200).json(updatedScore);
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Update a score via PATCH method.
     *
     * @param {String} id field           Score ID in MongoDB.
     * @param {Object} JSON body object   A JSON object with all the required properties to update a score via PATCH method.
     * @return                            The updated score object with its respective properties.
     */
    updateById: async (req, res, next) => {
        try {
            // Custom value created by the validator.
            const { id } = req.value.params;
            // The req.value.body may contain any number of fields.
            const newScore = req.value.body;
            const updatedScore = await Score.findByIdAndUpdate(id, newScore, { new: true });

            // Finish the process if no score was found.
            if (!updatedScore) {
                return res.status(404).json({
                    success: false,
                    message: `No score was found with id '${id}'.`
                });
            }

            // Return the updated document.
            return res.status(200).json(updatedScore);
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Delete a score.
     *
     * @param {String} id field   Score ID in MongoDB.
     * @return                    A JSON object with its respective details about the process.
     */
    destroyById: async (req, res, next) => {
        try {
            // Custom value created by the validator.
            const { id } = req.value.params;

            const score = await Score.findById(id);

            // Finish the process if no score was found.
            if (!score) {
                return res.status(404).json({
                    success: false,
                    message: `No score was found with id '${id}'. Perhaps it was already deleted!`
                });
            }

            // Delete the score in case of it has not been deleted yet.
            await Score.findByIdAndRemove(id);

            /**
             * 204 code may be appropriate if a document was deleted and it may be good for requests via ajax,
             * however the process was successfully performed and we need to inform it to the front.
             */
            return res.status(200).json({
                success: true,
                message: `The score with id '${id}' was successfully deleted!`
            });
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Delete multiple scores.
     *
     * @param {Object} filter field   Query strings object.
     * @return                        A JSON object with its respective details about the process.
     */
    destroy: async (req, res, next) => {
        try {
            const { filter } = req.query;

            let props;
            const query = {};

            const inclusion = includeDocs(filter);

            // Find score docs matching the property coming through filter.
            const scores = await Score.find({ [`${inclusion.prop}`]: { $exists: true } });

            // Finish the process if no scores were found with the specified property.
            if (scores.length === 0) {
                return res.status(404).json({
                    success: false,
                    message: `No scores were found with the property '${inclusion.prop}'.`
                });
            }

            // Removes the unnecessary -_id property added by the method.
            if (inclusion.props.match(/ -_id/i)) {
                props = inclusion.props.replace(/ -_id/i, '');
            }

            // Allow only a single property in order to create a regex for an specific field.
            if (typeof inclusion.prop !== 'string') {
                return res.status(400).json({
                    success: false,
                    message: 'The include method only supports a single property.'
                });
            }

            // Validate the regex value in order to avoid deleting the entire scores collection.
            if (!props) {
                return res.status(400).json({
                    success: false,
                    message: 'The scores deletion process cannot be completed due to there is not a value for the regex.'
                });
            }

            // Construct the query dynamically.
            query[`${inclusion.prop}`] = {
                $regex: props,
                $options: 'i'
            };

            // Remove all the docs matching the query.
            await Score.remove(query);

            /**
             * 204 code may be appropriate if a document was deleted and it may be good for requests via ajax,
             * however the process was successfully performed and we need to inform it to the front.
             */
            return res.status(200).json({
                success: true,
                message: `All the scores matching the regex '${props}' were successfully deleted!`
            });
        } catch (error) {
            return next(error);
        }
    }
};
