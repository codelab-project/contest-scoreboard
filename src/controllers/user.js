/**
 * Required modules.
 */
const User            = require('../models/user');
const { filterDocs }  = require('../helpers/filters');
const { includeDocs } = require('../helpers/inclusions');


/**
 * Export the User Controller.
 */
module.exports = {
    /**
     * Read (retrieve) users.
     *
     * @param {Object} filter field   Query strings object.
     * @return                        An array of filtered elements based on the query strings.
     */
    find: async (req, res, next) => {
        try {
            const { filter } = req.query;

            const users = await User.find();

            // Finish the process if no users were found.
            if (users.length === 0) {
                return res.status(404).json({
                    success: false,
                    message: 'No users were found.'
                });
            }

            const filteredUsers = filterDocs(users, filter);

            return res.status(200).json(filteredUsers);
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Read (retrieve) a single user.
     *
     * @param {String} id field   User ID in MongoDB.
     * @return                    A user object with its respective properties.
     */
    findById: async (req, res, next) => {
        try {
            const { filter } = req.query;

            // Custom field 'value' created by joi validator.
            const { id } = req.value.params;
            const user = await User.findById(id);

            // Finish the process if no user was found.
            if (!user) {
                return res.status(404).json({
                    success: false,
                    message: `No user was found with id '${id}'.`
                });
            }

            const filteredUser = filterDocs(user, filter);

            return res.status(200).json(filteredUser);
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Create a user or multiple users.
     *
     * @param {Object|Array} Object or Array   Can be either a single model instance or an array of instances.
     * @return                                 A user object or an array of users objects.
     */
    create: async (req, res, next) => {
        try {
            const user = await User.create(req.value.body);
            return res.status(201).json(user);
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Update a user via PUT method.
     *
     * @param {String} id field           User ID in MongoDB.
     * @param {Object} JSON body object   A JSON object with all the required properties to update a user via PUT method.
     * @return                            The updated user object with its respective properties.
     */
    replaceById: async (req, res, next) => {
        try {
            // Custom value created by the validator.
            const { id } = req.value.params;
            // Enforce that the req.value.body must contain all the fields.
            const newUser = req.value.body;
            const updatedUser = await User.findByIdAndUpdate(id, newUser, { new: true });

            // Finish the process if no user was found.
            if (!updatedUser) {
                return res.status(404).json({
                    success: false,
                    message: `No user was found with id '${id}'.`
                });
            }

            // Return the updated document.
            return res.status(200).json(updatedUser);
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Update a user via PATCH method.
     *
     * @param {String} id field           User ID in MongoDB.
     * @param {Object} JSON body object   A JSON object with all the required properties to update a user via PATCH method.
     * @return                            The updated user object with its respective properties.
     */
    updateById: async (req, res, next) => {
        try {
            // Custom value created by the validator.
            const { id } = req.value.params;
            // The req.value.body may contain any number of fields.
            const newUser = req.value.body;
            const updatedUser = await User.findByIdAndUpdate(id, newUser, { new: true });

            // Finish the process if no user was found.
            if (!updatedUser) {
                return res.status(404).json({
                    success: false,
                    message: `No user was found with id '${id}'.`
                });
            }

            // Return the updated document.
            return res.status(200).json(updatedUser);
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Delete a user.
     *
     * @param {String} id field   User ID in MongoDB.
     * @return                    A JSON object with its respective details about the process.
     */
    destroyById: async (req, res, next) => {
        try {
            // Custom value created by the validator.
            const { id } = req.value.params;

            const user = await User.findById(id);

            // Finish the process if no user was found.
            if (!user) {
                return res.status(404).json({
                    success: false,
                    message: `No user was found with id '${id}'. Perhaps it was already deleted!`
                });
            }

            // Delete the user in case of it has not been deleted yet.
            await User.findByIdAndRemove(id);

            /**
             * 204 code may be appropriate if a document was deleted and it may be good for requests via ajax,
             * however the process was successfully performed and we need to inform it to the front.
             */
            return res.status(200).json({
                success: true,
                message: `The user with id '${id}' was successfully deleted!`
            });
        } catch (error) {
            return next(error);
        }
    },

    /**
     * Delete multiple users.
     *
     * @param {Object} filter field   Query strings object.
     * @return                        A JSON object with its respective details about the process.
     */
    destroy: async (req, res, next) => {
        try {
            const { filter } = req.query;

            let props;
            const query = {};

            const inclusion = includeDocs(filter);

            // Find user docs matching the property coming through filter.
            const users = await User.find({ [`${inclusion.prop}`]: { $exists: true } });

            // Finish the process if no users were found with the specified property.
            if (users.length === 0) {
                return res.status(404).json({
                    success: false,
                    message: `No users were found with the property '${inclusion.prop}'.`
                });
            }

            // Removes the unnecessary -_id property added by the method.
            if (inclusion.props.match(/ -_id/i)) {
                props = inclusion.props.replace(/ -_id/i, '');
            }

            // Allow only a single property in order to create a regex for an specific field.
            if (typeof inclusion.prop !== 'string') {
                return res.status(400).json({
                    success: false,
                    message: 'The include method only supports a single property.'
                });
            }

            // Validate the regex value in order to avoid deleting the entire users collection.
            if (!props) {
                return res.status(400).json({
                    success: false,
                    message: 'The users deletion process cannot be completed due to there is not a value for the regex.'
                });
            }

            // Construct the query dynamically.
            query[`${inclusion.prop}`] = {
                $regex: props,
                $options: 'i'
            };

            // Remove all the docs matching the query.
            await User.remove(query);

            /**
             * 204 code may be appropriate if a document was deleted and it may be good for requests via ajax,
             * however the process was successfully performed and we need to inform it to the front.
             */
            return res.status(200).json({
                success: true,
                message: `All the users matching the regex '${props}' were successfully deleted!`
            });
        } catch (error) {
            return next(error);
        }
    }
};
