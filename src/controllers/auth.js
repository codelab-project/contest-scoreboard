/**
 * Required modules.
 */
const jwt  = require('jsonwebtoken');
const User = require('../models/user');


/**
 * Export the Auth Controller.
 */
module.exports = {
    /**
     * User sign up.
     *
     * @param {Object} Object   A JSON Object with the user properties for being signed up.
     * @return                  A JSON object with a user property which contains its respective details, eg: name, email and token.
     */
    signup: async (req, res, next) => {
        try {
            const user = await User.create(req.value.body);

            // User authentication object retrieved.
            const authentication = user.toAuthJSON();

            // Store the user authentication object as a session object.
            req.session.user = authentication;

            await User.findByIdAndUpdate(user.id, { sessionToken: authentication.token });

            return res.status(201).json(authentication);
        } catch (error) {
            return next(error);
        }
    },

    /**
     * User log in.
     *
     * @param {String} email field      E-mail belonging to the registered / created user.
     * @param {String} password field   Password belonging to the registered / created user.
     * @return                          A JSON object with a user property which contains its respective details, eg: name, email and token.
     */
    login: async (req, res, next) => {
        try {
            const { email, password } = req.value.body;

            const user = await User.findOne({ email });

            if (!user) {
                return res.status(404).json({
                    success: false,
                    message: `Unable to find a user with email '${email}'`
                });
            }

            if (!user.isValidPassword(password)) {
                return res.status(401).json({
                    success: false,
                    message: 'Your credentials are not valid!'
                });
            }

            const decodedToken = jwt.verify(user.sessionToken, process.env.JWT_SECRET, (error, decoded) => {
                if (error) {
                    next(error);
                }

                return decoded;
            });

            let authenticated = {};

            // Get the plain UTC time format as the 'exp' one from the JWT.
            // Otherwise the 'new Date().getTime()' will be converted to you local timezone
            // when is compared, which could be a different one than the jwt-issuer.
            const currentDate = new Date().getTime() / 1000;

            /**
             * Verify if the user session token has expired.
             *     If the token has expired
             *         create a new one and update the user doc.
             *     Otherwise
             *         use the same valid token and enable the user to perform the login.
             */
            if (decodedToken.exp < currentDate) {
                // Generate a new authentication token (retrieve the new authentication object).
                authenticated = user.toAuthJSON();

                const fieldsToUpdate = {
                    sessionExpired: false,
                    sessionToken: authenticated.token
                };

                // Update the user for being authenticated.
                await User.findByIdAndUpdate(user.id, fieldsToUpdate);
            } else {
                authenticated.name = user.name;
                authenticated.surname = user.surname;
                authenticated.email = user.email;
                authenticated.token = user.sessionToken;

                await User.findByIdAndUpdate(user.id, { sessionExpired: false });
            }

            // Store the user authentication object as a session object.
            req.session.user = authenticated;

            return res.status(200).json(authenticated);
        } catch (error) {
            return next(error);
        }
    },

    /**
     * User log out.
     *
     * @param {Object} user field   User property (object) within the req.session.
     * @return                      A JSON object with its respective details, eg: success, message.
     */
    logout: async (req, res, next) => {
        try {
            // Get the authorization header from the request headers, e.g:
            // { "Authorization": "Bearer <JSON-WEB-TOKEN>" }
            const header = req.headers.authorization;

            let token = '';

            if (header) {
                // Split the authorization header value (string) into array of strings
                // in order to get the token.
                token = header.split(' ')[1];
            }

            if (!token) {
                return res.status(401).json({
                    success: false,
                    message: 'No token found in order to perform the user log out!'
                });
            }

            // Get user stored in session in order to manage a logout via rest console.
            const { user } = req.session;

            if (token !== user.token) {
                return res.status(404).json({
                    success: false,
                    message: "The token is not valid! The log out request won't be performed."
                });
            }

            const email = user.email;

            await User.findOneAndUpdate({ email }, { sessionExpired: true });

            req.session.destroy();

            return res.status(200).json({
                success: true,
                message: `The user '${email}' was sucessfully logged out!`
            });
        } catch (error) {
            return next(error);
        }
    }
};
