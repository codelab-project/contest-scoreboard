/**
 * Required modules.
 */
const chai     = require('chai');
const chaiHttp = require('chai-http');
const faker    = require('faker');

chai.use(chaiHttp);


/**
 * User tests.
 */
describe('User Tests', () => {
    // These variables will be used along the user tests.
    let user        = {};
    let createdUser = {};

    // Test the user sign up.
    it('POST /auth/signup (sign up a new user in order to perform the tests)', done => {
        chai.request('http://localhost:8000')
            .post('/auth/signup')
            .set('content-type', 'application/json')
            .send({
                name: 'John Doe',
                email: 'john.doe@fsociety.com',
                password: 'john.doe',
                avatar: 'https://discordemoji.com/assets/emoji/fsociety.png'
            })
            .end((err, res) => {
                // There should not be errors.
                chai.expect(err).to.be.null;

                // There should be a 201 status code.
                chai.expect(res).to.have.status(201);

                // There should be an object as response.
                chai.expect(res.body).to.be.an('object');

                // There should be an object with the following properties and types:
                chai.expect(res.body).to.have.property('name').to.be.a('string');
                chai.expect(res.body).to.have.property('email').to.be.a('string');
                chai.expect(res.body).to.have.property('token').to.be.a('string');

                user = res.body;

                done();
            });
    });

    // Test the user creation.
    it('POST /users (create a user)', done => {
        chai.request('http://localhost:8000')
            .post('/users')
            .set({
                'content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            })
            .send({
                name: 'Harry Potter',
                email: 'harry.potter@hogwarts.com',
                password: 'test123'
            })
            .end((err, res) => {
                // There should be no errors.
                chai.expect(err).to.be.null;

                // There should be a 201 status code.
                chai.expect(res).to.have.status(201);

                // There should be an object as response.
                chai.expect(res.body).to.be.an('object');

                // These are the expected properties within the response object.
                chai.expect(res.body).to.have.property('_id').to.be.a('string');
                chai.expect(res.body).to.have.property('name').to.be.a('string');
                chai.expect(res.body).to.have.property('email').to.be.a('string');
                chai.expect(res.body).to.have.property('password').to.be.a('string');
                chai.expect(res.body).to.have.property('createdAt').to.be.a('string');
                chai.expect(res.body).to.have.property('updatedAt').to.be.a('string');

                // These are the expected values.
                chai.expect(res.body.name).to.be.equal('Harry Potter');
                chai.expect(res.body.email).to.be.equal('harry.potter@hogwarts.com');

                // Assign the created user to the reusable createdUser object.
                createdUser = res.body;

                done();
            });
    });

    // Test the multiple users creation.
    it('POST /users (create multiple users)', done => {
        const totalUsers = process.env.SEED_USERS || 10;

        const newUsers = [];
        let newsUser = {};
        let counter = 0;

        for (let i = 0; i < totalUsers; i++) {
            newsUser = {
                name: `${faker.name.firstName(2)} ${faker.name.lastName()}`,
                email: `${faker.internet.email()}`,
                password: 'nooneknows',
                avatar: `http://randomuser.me/api/portraits/women/${counter}.jpg`
            };

            newUsers.push(newsUser);

            counter++;
        }

        chai.request('http://localhost:8000')
            .post('/users')
            .set({
                'content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            })
            .send(newUsers)
            .end((err, res) => {
                // There should be no errors.
                chai.expect(err).to.be.null;

                // There should be a 201 status code.
                chai.expect(res).to.have.status(201);

                // There should be an object as response.
                chai.expect(res.body).to.be.an('array');

                // These are the expected properties within the response object.
                chai.expect(res.body[0]).to.have.property('_id').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('name').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('email').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('password').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('createdAt').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('updatedAt').to.be.a('string');

                done();
            });
    });

    // Test the user get method in order to get all the users.
    it('GET /users', done => {
        chai.request('http://localhost:8000')
            .get('/users')
            .set({
                'content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            })
            .end((err, res) => {
                // There should be no errors.
                chai.expect(err).to.be.null;

                // There should be a 200 status code.
                chai.expect(res).to.have.status(200);

                // There should be an array as response.
                chai.expect(res.body).to.be.an('array');

                done();
            });
    });

    // Test the user filter by email in order to get its id.
    it('GET /users (filter[where][email]=john.doe@fsociety.com)', done => {
        chai.request('http://localhost:8000')
            .get('/users')
            .set({
                'content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            })
            .query({ filter: { where: { email: 'john.doe@fsociety.com' } } })
            .end((err, res) => {
                // There should be no errors.
                chai.expect(err).to.be.null;

                // There should be a 200 status code.
                chai.expect(res).to.have.status(200);

                // There should be an array as response.
                chai.expect(res.body).to.be.an('array');

                // These are the expected properties within the only one object belonging to the array.
                chai.expect(res.body[0]).to.be.an('object');
                chai.expect(res.body[0]).to.have.property('_id').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('name').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('email').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('password').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('createdAt').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('updatedAt').to.be.a('string');

                // Assign the id to the reusable user object.
                user._id = res.body[0]._id;

                done();
            });
    });

    // Test the user filter with some specific fields.
    it('GET /users (filter[fields][name]=true&filter[fields][email]=true)', done => {
        chai.request('http://localhost:8000')
            .get('/users')
            .set({
                'content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            })
            .query({ filter: { fields: { name: true, email: true } } })
            .end((err, res) => {
                // There should be no errors.
                chai.expect(err).to.be.null;

                // There should be a 200 status code.
                chai.expect(res).to.have.status(200);

                // There should be an array as response.
                chai.expect(res.body).to.be.an('array');

                // These are the expected properties within the only one object belonging to the array.
                chai.expect(res.body[0]).to.be.an('object');
                chai.expect(res.body[0]).to.have.property('name').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('email').to.be.a('string');

                done();
            });
    });

    // Test the user filter in order to sort name ASC.
    it('GET /users (filter[order]=name ASC)', done => {
        chai.request('http://localhost:8000')
            .get('/users')
            .set({
                'content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            })
            .query({ filter: { order: 'name ASC' } })
            .end((err, res) => {
                // There should be no errors.
                chai.expect(err).to.be.null;

                // There should be a 200 status code.
                chai.expect(res).to.have.status(200);

                // There should be an array as response.
                chai.expect(res.body).to.be.an('array');

                // These are the expected properties within the only one object belonging to the array.
                chai.expect(res.body[0]).to.be.an('object');
                chai.expect(res.body[0]).to.have.property('_id').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('name').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('email').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('password').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('createdAt').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('updatedAt').to.be.a('string');

                done();
            });
    });

    // Test the user filter in order to limit the response docs to 1.
    it('GET /users (filter[limit]=1)', done => {
        chai.request('http://localhost:8000')
            .get('/users')
            .set({
                'content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            })
            .query({ filter: { limit: 1 } })
            .end((err, res) => {
                // There should be no errors.
                chai.expect(err).to.be.null;

                // There should be a 200 status code.
                chai.expect(res).to.have.status(200);

                // There should be an array as response.
                chai.expect(res.body).to.be.an('array');

                // These are the expected properties within the only one object belonging to the array.
                chai.expect(res.body[0]).to.be.an('object');
                chai.expect(res.body[0]).to.have.property('_id').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('name').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('email').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('password').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('createdAt').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('updatedAt').to.be.a('string');

                done();
            });
    });

    // Test the user filter in order to skip 4 docs.
    it('GET /users (filter[skip]=4)', done => {
        chai.request('http://localhost:8000')
            .get('/users')
            .set({
                'content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            })
            .query({ filter: { skip: 4 } })
            .end((err, res) => {
                // There should be no errors.
                chai.expect(err).to.be.null;

                // There should be a 200 status code.
                chai.expect(res).to.have.status(200);

                // There should be an array as response.
                chai.expect(res.body).to.be.an('array');

                // These are the expected properties within the only one object belonging to the array.
                chai.expect(res.body[0]).to.be.an('object');
                chai.expect(res.body[0]).to.have.property('_id').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('name').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('email').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('password').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('createdAt').to.be.a('string');
                chai.expect(res.body[0]).to.have.property('updatedAt').to.be.a('string');

                done();
            });
    });

    // Test the user get method by id.
    it('GET /users/:userId', done => {
        chai.request('http://localhost:8000')
            .get(`/users/${user._id}`)
            .set({
                'content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            })
            .end((err, res) => {
                // There should be no errors.
                chai.expect(err).to.be.null;

                // There should be a 200 status code.
                chai.expect(res).to.have.status(200);

                // There should be an object as response.
                chai.expect(res.body).to.be.an('object');

                // These are the expected properties within the response object.
                chai.expect(res.body).to.have.property('_id').to.be.a('string');
                chai.expect(res.body).to.have.property('name').to.be.a('string');
                chai.expect(res.body).to.have.property('email').to.be.a('string');
                chai.expect(res.body).to.have.property('password').to.be.a('string');
                chai.expect(res.body).to.have.property('createdAt').to.be.a('string');
                chai.expect(res.body).to.have.property('updatedAt').to.be.a('string');

                done();
            });
    });

    // Test the user update method with all its required properties.
    it('PUT /users/:userId (update the entire document - recently created user)', done => {
        chai.request('http://localhost:8000')
            .put(`/users/${createdUser._id}`)
            .set({
                'content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            })
            .send({
                name: 'Buzz Astronaut',
                email: 'buzz@toystory.com',
                password: 'buzz123',
                avatar: 'http://pm1.narvii.com/6488/7789292cf8f6dfd514b7bea8a1db3f425a0811a0_128.jpg'
            })
            .end((err, res) => {
                // There should be no errors.
                chai.expect(err).to.be.null;

                // There should be a 200 status code.
                chai.expect(res).to.have.status(200);

                // There should be an object as response.
                chai.expect(res.body).to.be.an('object');

                // These are the expected properties within the response object.
                chai.expect(res.body).to.have.property('_id').to.be.a('string');
                chai.expect(res.body).to.have.property('name').to.be.a('string');
                chai.expect(res.body).to.have.property('email').to.be.a('string');
                chai.expect(res.body).to.have.property('password').to.be.a('string');
                chai.expect(res.body).to.have.property('avatar').to.be.a('string');
                chai.expect(res.body).to.have.property('createdAt').to.be.a('string');
                chai.expect(res.body).to.have.property('updatedAt').to.be.a('string');

                // The new values must be the expected ones!
                chai.expect(res.body.name).to.be.equal('Buzz Astronaut');
                chai.expect(res.body.email).to.be.equal('buzz@toystory.com');
                chai.expect(res.body.avatar).to.be.equal('http://pm1.narvii.com/6488/7789292cf8f6dfd514b7bea8a1db3f425a0811a0_128.jpg');

                // Assign the updated user to the reusable createdUser object.
                createdUser = res.body;

                done();
            });
    });

    // Test the user update method with some properties.
    it('PATCH /users/:userId (update some filds belonging to the recently created user)', done => {
        chai.request('http://localhost:8000')
            .patch(`/users/${createdUser._id}`)
            .set({
                'content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            })
            .send({
                name: 'Woody Sheriff',
                email: 'woody@toystory.com',
                avatar: 'https://pm1.narvii.com/6284/118b8e68fac62ebc05fe0ccc5654d941cedc7ecd_128.jpg'
            })
            .end((err, res) => {
                // There should be no errors.
                chai.expect(err).to.be.null;

                // There should be a 200 status code.
                chai.expect(res).to.have.status(200);

                // There should be an object as response.
                chai.expect(res.body).to.be.an('object');

                // These are the expected properties within the response object.
                chai.expect(res.body).to.have.property('_id').to.be.a('string');
                chai.expect(res.body).to.have.property('name').to.be.a('string');
                chai.expect(res.body).to.have.property('email').to.be.a('string');
                chai.expect(res.body).to.have.property('password').to.be.a('string');
                chai.expect(res.body).to.have.property('avatar').to.be.a('string');
                chai.expect(res.body).to.have.property('createdAt').to.be.a('string');
                chai.expect(res.body).to.have.property('updatedAt').to.be.a('string');

                // The new values must be the expected ones!
                chai.expect(res.body.name).to.be.equal('Woody Sheriff');
                chai.expect(res.body.email).to.be.equal('woody@toystory.com');
                chai.expect(res.body.avatar).to.be.equal('https://pm1.narvii.com/6284/118b8e68fac62ebc05fe0ccc5654d941cedc7ecd_128.jpg');

                // Assign the updated user to the reusable createdUser object.
                createdUser = res.body;

                done();
            });
    });

    // Test the user deletion method by id.
    it('DELETE /users/:userId (delete the user created for testing purposes "John Doe")', done => {
        chai.request('http://localhost:8000')
            .delete(`/users/${createdUser._id}`)
            .set({
                'content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            })
            .end((err, res) => {
                // There should be no errors.
                chai.expect(err).to.be.null;

                // There should be a 201 status code.
                chai.expect(res).to.have.status(200);

                // There should be an object as response.
                chai.expect(res.body).to.be.an('object');

                // These are the expected properties within the response object.
                chai.expect(res.body).to.have.property('success').to.be.a('boolean');
                chai.expect(res.body).to.have.property('message').to.be.a('string');

                // The new values must be the expected ones!
                chai.expect(res.body.success).to.be.true;
                chai.expect(res.body.message).to.be.equal(`The user with id '${createdUser._id}' was successfully deleted!`);

                done();
            });
    });

    // Test the multiple users deletion method.
    it('DELETE /users (filter[include]=avatar&filter[include][avatar]=randomuser)', done => {
        chai.request('http://localhost:8000')
            .delete('/users')
            .set({
                'content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            })
            .query({ filter: { include: ['avatar', { avatar: 'randomuser' }] } })
            .end((err, res) => {
                // There should be no errors.
                chai.expect(err).to.be.null;

                // There should be a 201 status code.
                chai.expect(res).to.have.status(200);

                // There should be an object as response.
                chai.expect(res.body).to.be.an('object');

                // These are the expected properties within the response object.
                chai.expect(res.body).to.have.property('success').to.be.a('boolean');
                chai.expect(res.body).to.have.property('message').to.be.a('string');

                // The new values must be the expected ones!
                chai.expect(res.body.success).to.be.true;
                chai.expect(res.body.message).to.be.equal('All the users matching the regex \'randomuser\' were successfully deleted!');

                done();
            });
    });

    // Test the multiple users deletion method.
    it('DELETE /users (filter[include]=avatar&filter[include][avatar]=discordemoji)', done => {
        chai.request('http://localhost:8000')
            .delete('/users')
            .set({
                'content-type': 'application/json',
                Authorization: `Bearer ${user.token}`
            })
            .query({ filter: { include: ['avatar', { avatar: 'discordemoji' }] } })
            .end((err, res) => {
                // There should be no errors.
                chai.expect(err).to.be.null;

                // There should be a 201 status code.
                chai.expect(res).to.have.status(200);

                // There should be an object as response.
                chai.expect(res.body).to.be.an('object');

                // These are the expected properties within the response object.
                chai.expect(res.body).to.have.property('success').to.be.a('boolean');
                chai.expect(res.body).to.have.property('message').to.be.a('string');

                // The new values must be the expected ones!
                chai.expect(res.body.success).to.be.true;
                chai.expect(res.body.message).to.be.equal('All the users matching the regex \'discordemoji\' were successfully deleted!');

                done();
            });
    });
});
