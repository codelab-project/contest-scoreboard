/**
 * Compare two values.
 *
 * @param {*} val1 The 1st value
 * @param {*} val2 The 2nd value
 * @returns {number} 0: =, positive: >, negative <
 * @private
 */
function compare(val1, val2) {
    if (val1 == null || val2 == null) {
        // Either val1 or val2 is null or undefined
        return val1 == val2 ? 0 : NaN;
    }

    if (typeof val1 === 'number') {
        return val1 - val2;
    }

    if (typeof val1 === 'string') {
        return (val1 > val2) ? 1 : ((val1 < val2) ? -1 : (val1 == val2) ? 0 : NaN);
    }

    if (typeof val1 === 'boolean') {
        return val1 - val2;
    }

    if (val1 instanceof Date) {
        const result = val1 - val2;

        return result;
    }

    // Return NaN if we don't know how to compare.
    return (val1 == val2) ? 0 : NaN;
}

function testInEquality(example, val) {
    if ('gt' in example) {
        return compare(val, example.gt) > 0;
    }

    if ('gte' in example) {
        return compare(val, example.gte) >= 0;
    }

    if ('lt' in example) {
        return compare(val, example.lt) < 0;
    }

    if ('lte' in example) {
        return compare(val, example.lte) <= 0;
    }

    return false;
}

function toRegExp(pattern) {
    if (pattern instanceof RegExp) {
        return pattern;
    }

    let regex = '';

    /**
     * Escaping user input to be treated as a literal string within a regex.
     *
     * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#Writing_a_Regular_Expression_Pattern
     */
    pattern = pattern.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
    for (let i = 0, n = pattern.length; i < n; i++) {
        const char = pattern.charAt(i);

        if (char === '\\') {
            i++; // Skip to next char.

            if (i < n) {
                regex += pattern.charAt(i);
            }

            continue;
        } else if (char === '%') {
            regex += '.*';
        } else if (char === '_') {
            regex += '.';
        } else if (char === '.') {
            regex += '\\.';
        } else if (char === '*') {
            regex += '\\*';
        } else {
            regex += char;
        }
    }

    return regex;
}

function test(example, value) {
    if (typeof value === 'string' && (example instanceof RegExp)) {
        return value.match(example);
    }

    if (example === undefined) {
        return undefined;
    }

    if (typeof example === 'object' && example !== null) {
        // Ignore geo near filter.
        if (example.near) {
            return true;
        }

        if (example.inq) {
            // if (!value) return false;
            for (let i = 0; i < example.inq.length; i++) {
                if (example.inq[i] == value) {
                    return true;
                }
            }

            return false;
        }

        if ('neq' in example) {
            return compare(example.neq, value) !== 0;
        }

        if ('between' in example) {
            const gte = testInEquality({ gte: example.between[0] }, value);
            const lte = testInEquality({ lte: example.between[1] }, value);

            return (gte && lte);
        }

        if (example.like || example.nlike) {
            let like = example.like || example.nlike;

            if (typeof like === 'string') {
                like = toRegExp(like);
            }

            if (example.like) {
                return !!new RegExp(like).test(value);
            }

            if (example.nlike) {
                return !new RegExp(like).test(value);
            }
        }

        if (testInEquality(example, value)) {
            return true;
        }
    }

    // Not strict equality.
    return (example !== null ? example.toString() : example) == (value != null ? value.toString() : value);
}

function getValue(obj, path) {
    if (obj == null) {
        return undefined;
    }

    let val = obj;

    const keys = path.split('.');

    for (let i = 0, n = keys.length; i < n; i++) {
        val = val[keys[i]];

        if (val == null) {
            return val;
        }
    }

    return val;
}

function matchesFilter(obj, filter) {
    let pass = true;
    const where = filter.where;
    const keys = Object.keys(where);

    keys.forEach(key => {
        if (key === 'and' || key === 'or') {
            if (Array.isArray(where[key])) {
                if (key === 'and') {
                    pass = where[key].every(cond => applyFilter({ where: cond })(obj));
                    return pass;
                }

                if (key === 'or') {
                    pass = where[key].some(cond => applyFilter({ where: cond })(obj));
                    return pass;
                }
            }
        }

        if (!test(where[key], getValue(obj, key))) {
            pass = false;
        }
    });

    return pass;
}

function applyFilter(filter) {
    const where = filter.where;

    if (typeof where === 'function') {
        return where;
    }

    return function (obj) {
        return matchesFilter(obj, filter);
    };
}

function selectFields(fields) {
    // Map function.
    return function (obj) {
        let key;
        let val;
        const result = {};

        for (field in fields) {
            key = field;
            val = fields[field];

            // Query string values comes as strings.
            if (val == 'true') {
                result[key] = obj[key];
            }
        }

        return result;
    };
}

function sorting(a, b) {
    let undefinedA;
    let undefinedB;

    for (let i = 0, l = this.length; i < l; i++) {
        const aVal = getValue(a, this[i].key);
        const bVal = getValue(b, this[i].key);

        undefinedB = bVal === undefined && aVal !== undefined;
        undefinedA = aVal === undefined && bVal !== undefined;

        if (undefinedB || aVal > bVal) {
            return 1 * this[i].reverse;
        } if (undefinedA || aVal < bVal) {
            return -1 * this[i].reverse;
        }
    }

    return 0;
}

function normalizeOrder(filter) {
    let orders = filter.order;

    // Transform into an array.
    if (typeof orders === 'string') {
        if (filter.order.indexOf(',') > -1) {
            orders = filter.order.split(/,\s*/);
        } else {
            orders = [filter.order];
        }
    }

    orders.forEach((key, i) => {
        let reverse = 1;

        const m = key.match(/\s+(A|DE)SC$/i);

        if (m) {
            key = key.replace(/\s+(A|DE)SC/i, '');

            if (m[1].toLowerCase() === 'de') {
                reverse = -1;
            }
        } else {
            const Ctor = SyntaxError || Error;
            throw new Ctor('filter.order must include ASC or DESC');
        }

        orders[i] = { key, reverse };
    });

    return (filter.orders = orders);
}


/**
 * Document filters middleware.
 */
module.exports = {
    filterDocs: (docs, filter) => {
        if (filter) {
            // Perform some sorting.
            if (filter.order) {
                docs = docs.sort(sorting.bind(normalizeOrder(filter)));
            }

            // Apply some filtration.
            if (filter.where) {
                docs = docs ? docs.filter(applyFilter(filter)) : docs;
            }

            // Field selection.
            if (filter.fields) {
                docs = docs.map(selectFields(filter.fields));
            }

            // Get and set values for limit and skip.
            const skip  = filter.skip || filter.offset || 0;
            const limit = filter.limit || docs.length;

            docs = docs.slice(skip, skip + limit);
        }

        return docs;
    }
};
