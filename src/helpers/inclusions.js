/**
 * Document inclusions middleware.
 */
module.exports = {
    includeDocs: filter => {
        const response = {};

        if (filter) {
            let includeFilter;
            let includeValue;
            let prop = '';
            let selectProps = '';

            if (filter && filter.include) {
                includeFilter = filter.include;

                if (typeof include === 'string') {
                    includeValue = Array.of(includeFilter);
                } else {
                    includeValue = includeFilter;
                }

                // With the include property, e.g: filter[include]=propertyName
                if (typeof includeValue !== 'string' && includeValue.length > 1) {
                    includeValue.forEach(key => {
                        // Get the related property for being populated: filter[include]=propertyName
                        if (typeof key === 'string' && typeof key != 'undefined') {
                            prop = key;
                        }

                        if (typeof key === 'object') {
                            const values = Object.values(key);

                            // Construct the props for being selected through the object population.
                            values.forEach(value => {
                                // Take a single property.
                                if (typeof value === 'string') {
                                    selectProps = value;
                                }

                                // Take multiple properties.
                                if (typeof value === 'object') {
                                    selectProps = value.join(' ');
                                }
                            });
                        }
                    });
                } else { // Without the related property name through the include filter, just set the related object fields e.g: filter[include][propertyName]=fieldName
                    prop = includeValue;

                    if (typeof prop === 'string') {
                        selectProps = '';
                    }
                }
            }

            // Explicitly exclude the `_id` field if it is not required.
            if (selectProps.includes('_id') === false) {
                selectProps += ' -_id';
            }

            response.prop = prop;
            response.props = selectProps;
        }

        return response;
    }
};
