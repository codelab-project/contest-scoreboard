/**
 * Required modules.
 */
const fetch = require('node-fetch');


/**
 * Routes middleware.
 */
module.exports = {
    findGistById: gistId => {
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Bearer ${process.env.GITHUB_ACCESS_TOKEN}`
            }
        };

        return fetch(`${process.env.GITHUB_API_URL}/gists/${gistId}`, options)
            .then(res => res.json())
            .then(data => data)
            .catch(error => console.error(error))
        ;
    },

    getGistComments: gistId => {
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Bearer ${process.env.GITHUB_ACCESS_TOKEN}`
            }
        };

        return fetch(`${process.env.GITHUB_API_URL}/gists/${gistId}/comments`, options)
            .then(res => res.json())
            .then(data => data)
            .catch(error => console.error(error))
        ;
    },

    createGist: (userId, text) => {
        let gist = {
            description: `New contestant submission, score id: '${userId}'.`,
            files: {}
        };

        // Because no template method is allowed in order to create the file name dynamically!
        gist.files[`${userId}-submission.json`]         = {};
        gist.files[`${userId}-submission.json`].content = text;

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Bearer ${process.env.GITHUB_ACCESS_TOKEN}`
            },
            body: JSON.stringify(gist)
        };

        return fetch(`${process.env.GITHUB_API_URL}/gists`, options)
            .then(res => res.json())
            .then(data => data)
            .catch(error => console.error(error))
        ;
    },

    createGistComment: (gistId, comment) => {
        const gistComment = {
            body: comment
        };

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Bearer ${process.env.GITHUB_ACCESS_TOKEN}`
            },
            body: JSON.stringify(gistComment)
        };

        return fetch(`${process.env.GITHUB_API_URL}/gists/${gistId}/comments`, options)
            .then(res => res.json())
            .then(data => data)
            .catch(error => console.error(error))
        ;
    }
};
