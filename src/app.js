/**
 * Required modules.
 */
const express    = require('express');
const session    = require('express-session');
const bodyParser = require('body-parser');
const Promise    = require('bluebird');
const cors       = require('cors');
const dateformat = require('dateformat');
const dotenv     = require('dotenv');
const fs         = require('fs');
const helmet     = require('helmet');
const morgan     = require('morgan');
const mongoose   = require('mongoose');

// Create the express app.
const app = express();

// Global variables used along the project settings.
const hostPort = process.env.HOST_PORT || 8000;
const hostName = process.env.HOST_NAME || 'http://localhost';

// Dotenv initialization.
dotenv.config();

// Mongoose.
mongoose.Promise = Promise;
mongoose.connect(process.env.MONGODB_URL, { useNewUrlParser: true });
mongoose.set('useCreateIndex', true);


/**
 * Enable cors requests.
 */
app.use(cors({
    origin: process.env.CORS_WHITELIST.split(','),
    methods: 'GET, POST, PUT, PATCH, DELETE',
    allowedHeaders: 'Origin, X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization',
    maxAge: 600,        // Cache results of a preflight request for 10 minutes.
    credentials: true   // Enable set cookie.
}));

// Helmet middleware.
app.use(helmet());

// Morgan middleware.
morgan.format('myDate', () => dateformat(new Date(), 'yyyy-mm-dd HH:MM:ss'));

app.use(morgan('[:myDate] :method :url :status :req[header] :res[header] :remote-addr :response-time[0] ms'));
require('console-stamp')(console, {
    pattern: 'yyyy-mm-dd HH:MM:ss',
    metadata: () => {
        const used = process.memoryUsage().heapUsed / 1024 / 1024;
        return (`[${Math.round(used * 100) / 100} MB]`);
    },
    colors: {
        stamp: 'yellow',
        label: 'white',
        metadata: 'green'
    }
});

// Body parser middleware.
app.use(bodyParser.json());

// Session middleware.
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: true
}));

// Routes are being included dynamically.
fs.readdirSync(`${__dirname}/routes`).forEach(file => {
    if (file.substr(-3) === '.js') {
        const routeFile = require(`./routes/${file}`);
        app.use(routeFile);
    }
});

// Catch 404 errors and forward them to the error handler.
app.use((req, res, next) => {
    const err = new Error('Page not found.');
    err.status = 404;
    next(err);
});

// Development error handler function.
// Will print stack trace.
if (process.env.NODE_ENV === 'development') {
    app.use((error, req, res, next) => {
        // Response to command line.
        console.error(error);

        // Response to client.
        res.status(error.status || 500).json({
            message: error.message,
            error
        });
    });
}

// Production error handler function.
// No stack traces leaked to user.
if (process.env.NODE_ENV === 'production') {
    app.use((error, req, res, next) => {
        res.status(error.status || 500).json({
            message: error.message,
            error: {}
        });
    });
}

// Start the server.
app.listen(hostPort, () => console.log(`Server is running on: ${hostName}:${hostPort}`));
