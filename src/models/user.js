/**
 * Required modules.
 */
const mongoose    = require('mongoose');
const bcrypt      = require('bcrypt');
const jwt         = require('jsonwebtoken');
const Schema      = mongoose.Schema;
const SALT_FACTOR = 10;


/**
 * User schema
 */
const userSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        index: true
    },
    password: {
        type: String,
        required: true
    },
    sessionExpired: {
        type: Boolean,
        default: false
    },
    sessionToken: {
        type: String,
        default: ''
    },
    avatar: String
}, {
    // It sets automatically the fields: createdAt and updatedAt for each user document.
    timestamps: true
});


/**
 * User schema methods.
 */
userSchema.pre('save', function (next) {
    const user = this;

    if (!user.isModified('password')) {
        return next();
    }

    return bcrypt.genSalt(SALT_FACTOR, (error, salt) => {
        if (error) {
            return next(error);
        }

        return bcrypt.hash(user.password, salt, (err, hash) => {
            if (err) {
                return next(err);
            }

            user.password = hash;

            return next();
        });
    });
});

userSchema.methods = {
    isValidPassword(password) {
        return bcrypt.compareSync(password, this.password);
    },

    generateJWT() {
        return jwt.sign({
            iss: process.env.ISSUER,
            sub: this.id,
            iat: new Date().getTime(),                          // Current time.
            exp: new Date().setDate(new Date().getDate() + 1)   // Current time + 1 day ahead.
        }, process.env.JWT_SECRET);
    },

    toAuthJSON() {
        return {
            name: this.name,
            email: this.email,
            token: this.generateJWT()
        };
    }
};


/**
 * The schema is useless so far, then I create a model to use it.
 * Mongoose will create a collection named users (plural) for the first specified parameter.
 */
const user = mongoose.model('user', userSchema);


/**
 * Making this module available for the Node App.
 */
module.exports = user;
