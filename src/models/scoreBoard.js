/**
 * Required modules.
 */
const mongoose = require('mongoose');
const Schema   = mongoose.Schema;


/**
 * Scoreboard schema.
 */
const scoreboardSchema = new Schema({
    calculation: {
        type: Number,
        required: true
    },
    score: {
        type: Schema.Types.ObjectId,
        ref: 'score'
    }
}, {
    // It sets automatically the fields: createdAt and updatedAt for each score document.
    timestamps: true
});


/**
 * The schema is useless so far, then I create a model to use it.
 * Mongoose will create a collection named scoreboards (plural) for the first specified parameter.
 */
const scoreboard = mongoose.model('scoreboard', scoreboardSchema);


/**
 * Making this module available for the Node App.
 */
module.exports = scoreboard;
