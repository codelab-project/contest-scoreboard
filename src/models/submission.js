/**
 * Required modules.
 */
const mongoose = require('mongoose');
const Schema   = mongoose.Schema;


/**
 * Submission schema.
 */
const submissionSchema = new Schema({
    contestantNumber: {
        type: Number,
        required: true
    },
    solutions: {
        type: Number,
        required: true
    },
    timeSpent: {
        type: Number,
        required: true
    },
    submissionType: {
        type: String,
        required: true
    }
}, {
    // It sets automatically the fields: createdAt and updatedAt for each submission document.
    timestamps: true
});


/**
 * The schema is useless so far, then I create a model to use it.
 * Mongoose will create a collection named submissions (plural) for the first specified parameter.
 */
const submission = mongoose.model('submission', submissionSchema);


/**
 * Making this module available for the Node App.
 */
module.exports = submission;
