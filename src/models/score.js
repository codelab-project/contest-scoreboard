/**
 * Required modules.
 */
const mongoose = require('mongoose');
const Schema   = mongoose.Schema;


/**
 * Score schema.
 */
const scoreSchema = new Schema({
    calculation: Number,
    contestant: {
        type: Number,
        required: true
    },
    gistId: String,
    submissions: [{
        type: Schema.Types.ObjectId,
        ref: 'submission'
    }]
}, {
    // It sets automatically the fields: createdAt and updatedAt for each score document.
    timestamps: true
});


/**
 * The schema is useless so far, then I create a model to use it.
 * Mongoose will create a collection named scores (plural) for the first specified parameter.
 */
const score = mongoose.model('score', scoreSchema);


/**
 * Making this module available for the Node App.
 */
module.exports = score;
