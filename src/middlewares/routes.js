/**
 * Required modules.
 */
const Joi = require('joi');


/**
 * Routes middleware.
 */
module.exports = {
    // Generic method to validate a single param coming within the req.params.
    validateParam: (schema, name) => (req, res, next) => {
        const result = Joi.validate({ param: req.params[name] }, schema);

        if (result.error) {
            // Response to the controller with the error found.
            return res.status(400).json(result.error);
        }

        if (!req.value) {
            req.value = {};
        }

        if (!req.value.params) {
            req.value.params = {};
        }

        req.value.params[name] = result.value.param;

        // Allows the controller goes on after the field validation.
        return next();
    },

    // Generic method to validate the params coming within the req.body.
    validateBody: schema => (req, res, next) => {
        const result = Joi.validate(req.body, schema);

        if (result.error) {
            // Response to the controller with the error found.
            return res.status(400).json(result.error);
        }
        if (!req.value) {
            req.value = {};
        }

        if (!req.value.body) {
            req.value.body = {};
        }

        req.value.body = result.value;

        // Allows the controller goes on after the field validation.
        return next();
    },

    // List of schemas in order to perform the validation for each request.
    schemas: {
        // May be used for validating a MongoDB ID.
        mongoIdSchema: Joi.alternatives().try(
            Joi.object().keys({
                param: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
            }),

            Joi.object().keys({
                id: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
            })
        ),

        // May be used for validating a MongoDB ID.
        gistIdSchema: Joi.alternatives().try(
            Joi.object().keys({
                param: Joi.string().regex(/^[0-9a-fA-F]{32}$/).required()
            }),

            Joi.object().keys({
                id: Joi.string().regex(/^[0-9a-fA-F]{32}$/).required()
            })
        ),

        // May be used for POST and PUT methods.
        userSchema: Joi.alternatives().try(
            Joi.object().keys({
                name: Joi.string().required(),
                email: Joi.string().email().required(),
                password: Joi.string().required(),
                avatar: Joi.string()
            }),

            Joi.array().items([
                Joi.object().keys({
                    name: Joi.string().required(),
                    email: Joi.string().email().required(),
                    password: Joi.string().required(),
                    avatar: Joi.string()
                })
            ])
        ),

        userAuthentication: Joi.object().keys({
            email: Joi.string().email().required(),
            password: Joi.string().required()
        }),

        // May be used for the PATCH method.
        userOptionalSchema: Joi.object().keys({
            name: Joi.string(),
            email: Joi.string().email(),
            password: Joi.string(),
            avatar: Joi.string()
        }),

        // May be used for POST and PUT methods.
        scoreSchema: Joi.alternatives().try(
            Joi.object().keys({
                submission: Joi.string().required()
            }),

            Joi.array().items([
                Joi.object().keys({
                    submission: Joi.string().required()
                })
            ])
        ),

        // May be used for POST and PUT methods.
        scoreboardSchema: Joi.alternatives().try(
            Joi.object().keys({
                score: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
            }),

            Joi.array().items([
                Joi.object().keys({
                    score: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
                })
            ])
        ),

        // May be used for POST and PUT methods.
        gistSchema: Joi.alternatives().try(
            Joi.object().keys({
                score: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
                submission: Joi.string().required()
            }),

            Joi.array().items([
                Joi.object().keys({
                    score: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
                    submission: Joi.string().required()
                })
            ])
        ),

        // May be used for POST and PUT methods.
        gistCommentSchema: Joi.alternatives().try(
            Joi.object().keys({
                submission: Joi.string().required()
            }),

            Joi.array().items([
                Joi.object().keys({
                    submission: Joi.string().required()
                })
            ])
        )
    }
};
