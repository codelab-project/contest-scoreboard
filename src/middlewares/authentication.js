/**
 * Required modules.
 */
const jwt   = require('jsonwebtoken');
const Users = require('../models/user');


/**
 * User authentication middleware (bearer token).
 */
module.exports = {
    isUserAuthenticated: async (req, res, next) => {
        try {
            // Get the authorization header from the request headers, e.g:
            // { "Authorization": "Bearer <JSON-WEB-TOKEN>" }
            const header = req.headers.authorization;

            let token = '';

            if (header) {
                // Split the authorization header value (string) into array of strings
                // in order to get the token.
                token = header.split(' ')[1];
            }

            if (!token) {
                return res.status(401).json({
                    success: false,
                    message: 'No token found! You are not authorized to perform this operation!'
                });
            }

            const decodedToken = jwt.verify(token, process.env.JWT_SECRET, (error, decoded) => {
                if (error) {
                    return res.status(401).json({
                        success: false,
                        message: 'Token is not valid! You are not authorized to perform this operation!'
                    });
                }

                return decoded;
            });

            const user = await Users.findById(decodedToken.sub);

            // Re-validate the user in case of it was deleted from the db.
            if (!user) {
                return res.status(404).json({
                    success: false,
                    message: 'User not found! The access token is not valid!'
                });
            }

            // Get the plain UTC time format as the 'exp' one from the JWT.
            // Otherwise the 'new Date().getTime()' will be converted to you local timezone
            // when is compared, which could be a different one than the jwt-issuer.
            const currentDate = new Date().getTime() / 1000;

            // Rebuild the user session object if it has not been logged out when the API was restarted.
            // It'll be useful in order to perform a log out and update the user session status.
            if (!req.session.user && user.sessionExpired === false) {
                const authenticated = {
                    name: user.name,
                    email: user.email,
                    token: user.sessionToken
                };

                req.session.user = authenticated;
            }

            if (user.sessionExpired === true || decodedToken.exp < currentDate) {
                return res.status(401).json({
                    success: false,
                    message: 'Your session or access token has expired! You are not authorized to perform this operation!'
                });
            }

            next();
        } catch (error) {
            return next(error);
        }
    }
};
