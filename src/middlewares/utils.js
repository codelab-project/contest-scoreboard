/**
 * Exports the Scores Controller.
 */
module.exports = {
    /**
     * Verify submission string (properties).
     *
     * @param {Object} req.value.body field   Request body object.
     * @return                                A sanitized json object with its respective properties.
     */
    verifySubmission: (req, res, next) => {
        try {
            // Take the submission.
            const { submission } = req.value.body;

            // Sanitized submission for being used as response.
            const sanitized = {};

            // Regex for submission types.
            const submissionTypeRegex = /^[CIRUE]$/;

            /**
             * Take the string coming through the submission property,
             * then split it using the empty space string as separator.
             */
            const string = submission.split(' ');

            sanitized.contestantNumber = Number(string[0]);
            sanitized.solutions        = Number(string[1]);
            sanitized.timeSpent        = Number(string[2]);
            sanitized.submissionType   = string[3];

            if (sanitized.contestantNumber < 1 || sanitized.contestantNumber > 100) {
                return res.status(400).json({
                    success: false,
                    message: `Wrong contestant number '${sanitized.contestantNumber}'! The number must be greater than or equal to 1 and lower than or equal to 100.`
                });
            }

            if (sanitized.solutions < 1 || sanitized.solutions > 9) {
                return res.status(400).json({
                    success: false,
                    message: `Wrong solution number '${sanitized.solutions}'! The number must be greater than or equal to 1 and lower than or equal to 9.`
                });
            }

            if (sanitized.timeSpent < 1) {
                return res.status(400).json({
                    success: false,
                    message: `Wrong value for time spent '${sanitized.timeSpent}'! The value must be greater than or equal to 1, it indicates the time in minutes.`
                });
            }

            if (!sanitized.submissionType.match(submissionTypeRegex)) {
                return res.status(400).json({
                    success: false,
                    message: `Wrong submission type '${sanitized.submissionType}'! Allowed values are: {C | I | R | U | E}.`
                });
            }

            req.submission = sanitized;

            next();
        } catch (error) {
            return next(error);
        }
    }
};
