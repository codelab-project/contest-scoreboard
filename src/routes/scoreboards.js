/**
 * Required modules.
 *
 * Express Promise Router:
 * Unlike the default router, this module contains the try catch mechanism
 * without the need of defining it explicitly in the controller all the time.
 */
const router                = require('express-promise-router')();
const scoreboardsController = require('../controllers/scoreboards');
const { isUserAuthenticated } = require('../middlewares/authentication');
const {
    validateParam,
    validateBody,
    schemas
} = require('../middlewares/routes');


/**
 * ALL scoreboard routes must be authenticated first.
 */
router.all('/scoreboards*', isUserAuthenticated);


/**
 * Working with multiple scores.
 */
router.route('/scoreboards')
    .get(scoreboardsController.find)
    .post(validateBody(schemas.scoreboardSchema), scoreboardsController.create)
    .put(validateBody(schemas.scoreboardSchema), scoreboardsController.create)
    .delete(scoreboardsController.destroy)
;

/**
 * Working with a single score.
 */
router.route('/scoreboards/:id')
    .get(validateParam(schemas.mongoIdSchema, 'id'), scoreboardsController.findById)
    .delete(validateParam(schemas.mongoIdSchema, 'id'), scoreboardsController.destroyById)
;

// Exports the router.
module.exports = router;
