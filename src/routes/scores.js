/**
 * Required modules.
 *
 * Express Promise Router:
 * Unlike the default router, this module contains the try catch mechanism
 * without the need of defining it explicitly in the controller all the time.
 */
const router           = require('express-promise-router')();
const scoresController = require('../controllers/scores');
const { isUserAuthenticated } = require('../middlewares/authentication');
const { verifySubmission } = require('../middlewares/utils');
const {
    validateParam,
    validateBody,
    schemas
} = require('../middlewares/routes');


/**
 * ALL score routes must be authenticated first.
 */
router.all('/scores*', isUserAuthenticated);


/**
 * Working with multiple scores.
 */
router.route('/scores')
    .get(scoresController.find)
    .post(validateBody(schemas.scoreSchema), verifySubmission, scoresController.create)
    .put(validateBody(schemas.scoreSchema), verifySubmission, scoresController.create)
    .delete(scoresController.destroy)
;

/**
 * Working with a single score.
 */
router.route('/scores/:id')
    .get(validateParam(schemas.mongoIdSchema, 'id'), scoresController.findById)

    // Idempotent method used to replace an entire document, all its required parameters are required!
    .put([
        validateParam(schemas.mongoIdSchema, 'id'),
        validateBody(schemas.scoresController)
    ], verifySubmission, scoresController.replaceById)

    // Method used to update one or a mix of parameters belonging to a document.
    .patch([
        validateParam(schemas.mongoIdSchema, 'id'),
        validateBody(schemas.scoreSchema)
    ], verifySubmission, scoresController.updateById)

    .delete(validateParam(schemas.mongoIdSchema, 'id'), scoresController.destroyById)
;

// Exports the router.
module.exports = router;
