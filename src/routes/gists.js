/**
 * Required modules.
 *
 * Express Promise Router:
 * Unlike the default router, this module contains the try catch mechanism
 * without the need of defining it explicitly in the controller all the time.
 */
const router          = require('express-promise-router')();
const gistsController = require('../controllers/gists');
const { isUserAuthenticated } = require('../middlewares/authentication');
const { verifySubmission } = require('../middlewares/utils');
const {
    validateParam,
    validateBody,
    schemas
} = require('../middlewares/routes');


/**
 * ALL gists routes must be authenticated first.
 */
router.all('/gists*', isUserAuthenticated);


/**
 * Working with multiple gists.
 */
router.route('/gists')
//    .get(gistsController.find)
    .post(validateBody(schemas.gistSchema), verifySubmission, gistsController.create)
    .put(validateBody(schemas.gistSchema), verifySubmission, gistsController.create)
//    .delete(gistsController.destroy)
;

/**
 * Working with a single gist.
 */
router.route('/gists/:id/comments')
    .post([
        validateParam(schemas.mongoIdSchema, 'id'),
        validateBody(schemas.gistCommentSchema)
    ], verifySubmission, gistsController.createGistComment)
    .put([
        validateParam(schemas.mongoIdSchema, 'id'),
        validateBody(schemas.gistCommentSchema)
    ], verifySubmission, gistsController.createGistComment)
;

// Working with gist id on github.
router.route('/gists/:gistId/comments')
    .get(validateParam(schemas.gistIdSchema, 'gistId'), gistsController.getGistComments)
;

// Exports the router.
module.exports = router;
