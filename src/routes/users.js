/**
 * Required modules.
 *
 * Express Promise Router:
 * Unlike the default router, this module contains the try catch mechanism
 * without the need of defining it explicitly in the controller all the time.
 */
const router         = require('express-promise-router')();
const userController = require('../controllers/user');
const { isUserAuthenticated } = require('../middlewares/authentication');
const {
    validateParam,
    validateBody,
    schemas
} = require('../middlewares/routes');


/**
 * ALL user routes must be authenticated first.
 */
router.all('/users*', isUserAuthenticated);


/**
 * Working with multiple users.
 */
router.route('/users')
    .get(userController.find)
    .post(validateBody(schemas.userSchema), userController.create)
    .put(validateBody(schemas.userSchema), userController.create)
    .delete(userController.destroy)
;

/**
 * Working with a single user.
 */
router.route('/users/:id')
    .get(validateParam(schemas.mongoIdSchema, 'id'), userController.findById)

    // Idempotent method used to replace an entire document, all its required parameters are required!
    .put([
        validateParam(schemas.mongoIdSchema, 'id'),
        validateBody(schemas.userSchema)
    ], userController.replaceById)

    // Method used to update one or a mix of parameters belonging to a document.
    .patch([
        validateParam(schemas.mongoIdSchema, 'id'),
        validateBody(schemas.userOptionalSchema)
    ], userController.updateById)

    .delete(validateParam(schemas.mongoIdSchema, 'id'), userController.destroyById)
;

// Exports the router.
module.exports = router;
