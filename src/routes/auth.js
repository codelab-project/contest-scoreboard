/**
 * Required modules.
 *
 * Express Promise Router:
 * Unlike the default router, this module contains the try catch mechanism
 * without the need of defining it explicitly in the controller all the time.
 */
const router         = require('express-promise-router')();
const authController = require('../controllers/auth');
const {
    validateBody,
    schemas
} = require('../middlewares/routes');


/**
 * POST /auth/login
 */
router.route('/auth/login').post(validateBody(schemas.userAuthentication), authController.login);

/**
 * GET /auth/logout
 */
router.route('/auth/logout').get(authController.logout);

/**
 * POST /auth/signup
 */
router.route('/auth/signup').post(validateBody(schemas.userSchema), authController.signup);

// Exports the router.
module.exports = router;
