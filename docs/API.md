API Documentation
-----------------

## Table of Contents

* [Getting started](#getting-started)
    * [Request client](#request-client)
* [Working with data](#working-with-data)
    * [User authorization](#user-authorization)
    * [Perform requests](#perform-requests)
    * [Creating, updating, and deleting data](#creating-updating-and-deleting-data)
        * [Creating data (model instances)](#creating-data-model-instances)
        * [Updating data (model instances)](#updating-data-model-instances)
        * [Deleting data (model instances)](#deleting-data-model-instances)
    * [Querying data](#querying-data)
        * [Fields filter](#fields-filter)
        * [Include filter](#include-filter)
        * [Limit filter](#limit-filter)
        * [Order filter](#order-filter)
        * [Skip filter](#skip-filter)
        * [Where filter](#where-filter)
* [Additional resources](#additional-resources)


## Getting started

### Request client

There are a lot of alternatives in order to perform Restful API Requests through a Rest Client. [Postman](https://www.getpostman.com/postman "Postman Official Website") is a very good alternative for that.

The project listens on port **3000** and uses the ***localhost*** by default, so all the request may be performed through: http://localhost:8000. These parameters may be configured through the ```.env``` file.


## Working with data

### User authorization

> **Heads up**!
>
> This small RESTful API requires a user validation (**jwt**) in order to perform any request.
> You must perform the login method in order to get the user token which must set via header e.g: ```Authorization: Bearer userToken```.

#### User sign up

In order to perform a log in, you should have an account, if you have not created it yet, you are encouraged to create one!

In postman create a new POST request tab with the following settings:

| REST                                | Headers type             | Body type                |
|-------------------------------------|--------------------------|--------------------------|
| `http://localhost:8000/auth/signup` | JSON based format `(*)`. | JSON based format `(*)`. |

#### Headers example:

```
{
    Content-Type: application/json
}
```

#### Body example:

```json
{
    "name": "John Doe",
    "email": "john.doe@fsociety.com",
    "password": "john.doe"
}
```

#### Get the user authorization token

In postman create a new POST request tab with the following settings:

| REST                               | Headers type             | Body type                |
|------------------------------------|--------------------------|--------------------------|
| `http://localhost:8000/auth/login` | JSON based format `(*)`. | JSON based format `(*)`. |

#### Headers example:

```
{
    Content-Type: application/json
}
```

#### Body example:

```json
{
    "email": "john.doe@fsociety.com",
    "password": "john.doe"
}
```

Then perform the request clicking on the send button. If the process was successfully performed (the user exists in the database), the server will respond with an object (user), which includes the user _name_, _email_ and its respective _token_, e.g:

```json
{
    "name": "John Doe",
    "email": "john.doe@fsociety.com",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...."
}
```

Otherwise (if the user doesn't exist), the server will inform you as follows:

```json
{
    "success": false,
    "message": "Your credentials are not valid!"
}
```

### Perform requests

Once you've been authenticated, then you can use create, read, update, and delete (CRUD) operations to add data to the model, manipulate the data, and query it.

| Operation        | REST               | Model method (Node API)* | Corresponding SQL Operation |
|------------------|--------------------|--------------------------|-----------------------------|
| Create           | PUT /_modelNameInPlural_               | `create()*`      | INSERT          |
|                  | POST /_modelNameInPlural_              |                  |                 |
| Read (retrieve)  | GET /_modelNameInPlural?filter=..._    | `find()*`        | SELECT          |
|                  | GET /_modelNameInPlural/documentId_    | `findById()*`    |                 |
| Update (modify)  | PUT /_modelNameInPlural/documentId_    | `replaceById()*` | UPDATE          |
|                  | PATCH /_modelNameInPlural/documentId_  | `updateById()*`  |                 |
| Delete (Destroy) | DELETE /_modelNameInPlural?filter=..._ | `destroy()*`     | DELETE          |
|                  | DELETE /_modelNameInPlural/documentId_ | `destroyById()*` |                 |

### Creating, updating, and deleting data

Model data is also called a _model instance_; in database terminology, conceptually a model corresponds to a table, and a model instance corresponds to a _row_ or _record_ in the table.

#### Creating data (model instances)

| Method | REST                                      | Description      | Additional info        |
|--------|-------------------------------------------|------------------|------------------------|
| POST   | `http://localhost:8000/modelNameInPlural` | Create a *single model instance* or a *collection of model instances*. | JSON based `object*` or `array of objects*`. |
| PUT    | `http://localhost:8000/modelNameInPlural` | Create a *single model instance* or a *collection of model instances*. | JSON based `object*` or `array of objects*`. |

#### Object examples:

User model instance:

```json
{
    "name": "John Doe",
    "email": "john.doe@fsociety.com",
    "password": "anAmazingPassword",
    "avatar": "https://discordemoji.com/assets/emoji/fsociety.png"
}
```

Score model instance:

```json
{
    "submission": "3 1 11 C"
}
```

Gist model instance:

> **Note**!
>
> When a gist / gist comment is created or updated, its respective scores is updated too!

```json
{
    "score": "5bb2c69a3c21a144f62d7f23",
    "submission": "1 5 60 C"
}
```

Gist comment model instance:

```json
{
    "submission": "1 2 10 I"
}
```

#### Array of objects examples:

User model instances:

```json
[
    {
        "name": "John Doe",
        "email": "john.doe@fsociety.com",
        "password": "anAmazingPassword",
        "avatar": "https://discordemoji.com/assets/emoji/fsociety.png"
    },
    {
        "name": "Jenni Doe",
        "email": "jenni.doe@fsociety.com",
        "password": "anAmazingPassword",
        "avatar": "https://discordemoji.com/assets/emoji/fsociety.png"
    }
]
```

#### Updating data (model instances)

| Method | REST                                                      | Description | Additional info |
|--------|-----------------------------------------------------------|------------ |---------------- |
| PUT    | `http://localhost:8000/modelNameInPlural/modelInstanceId` | Update (all the required properties) a *single model instance*. | The _modelInstanceId`(*)`_ should be a MongoDB ID. |
| PATCH  | `http://localhost:8000/modelNameInPlural/modelInstanceId` | Update (one or a mix of properties) a *single model instance*.  | The _modelInstanceId`(*)`_ should be a MongoDB ID. |

#### Deleting data (model instances)

| Method | REST                                                      | Description | Additional info |
|--------|-----------------------------------------------------------|-------------|-----------------|
| DELETE | `http://localhost:8000/modelNameInPlural?filter=...` | Delete _multiple model instances_ matching a regex via filters. | The filter `property(*)` should be included through the include parameter, then a `value(*)` for that property should be included for being matched by the regex.
|        | `http://localhost:8000/modelNameInPlural/modelInstanceId` | Delete a _single model instance_. | The _modelInstanceId(*)_ should be a MongoDB ID. |

#### Example for deleting multiple model instances:

This is RESTful URL in order to DELETE multiple model instances via postman.

```
http://localhost:8000/users?filter[include]=avatar&filter[include][avatar]=randomuser
```

### Querying data


#### Overview

A query is a read operation on models that returns a set of data or results. You can query models using a Node API and a REST API, using filters. Filters specify criteria for the returned data set. The capabilities and options of the two APIs are the same–the only difference is the syntax used in HTTP requests versus Node function calls. In both cases, the API models return JSON.

> **Important**:
>
> A REST query must include the literal string “filter” in the URL query string. The Node API call does not include the literal string “filter” in the JSON.

> **Tip**:
>
> If you are trying query filters with curl, use the -g or --globoff  option to use brackets [ and ] in request URLs.

#### Examples

An example of using the ```find()``` method with both a _where_ and a _limit_ filter:

```javascript
User.find({where: {name: 'John'}, limit: 3}, function(err, accounts) { /* ... */ });
```

Equivalent using REST:

```
/users?filter[where][name]=John&filter[limit]=3
```

In both REST and Node API, you can use any number of filters to define a query. This API supports a specific filter syntax: it’s a lot like SQL, but designed specifically to serialize safely without injection and to be native to JavaScript.

The following table describes the API’s filter types:

| Filter type   | Type                     | Description                                                |
|---------------|--------------------------|------------------------------------------------------------|
| fields        | Object, Array, or String | Specify fields to include in or exclude from the response. |
| include       | String, Object, or Array | Include results from related models, e.g: *populate*.      |
| limit         | Number                   | Limit the number of instances to return.                   |
| order         | String                   | Specify sort order: ascending or descending.               |
| skip (offset) | Number                   | Skip the specified number of instances.                    |
| where         | Object                   | Specify search criteria; similar to a WHERE clause in SQL. |

#### REST syntax

Specify filters in the HTTP query string:

```
?filter_filterType_=_spec_&_filterType_=_spec_....
```

The number of filters that you can apply to a single request is limited only by the maximum URL length, which generally depends on the client used.

> **Important**:
>
> There is no equal sign after ?filter in the query string; e.g: ```http://localhost:8000/users?filter[where][name]=John Doe```

#### Node syntax

Specify filters as the first argument to ```find()```:

```
{ filterType: spec, filterType: spec, ... }
```

There is no theoretical limit on the number of filters you can apply.

Where:

- _filterType_ is the filter: **where**, **include**, **order**, **limit**, **skip**, or **fields**.
- _spec_ is the specification of the filter: for example for a _where_ filter, this is a logical condition that the results must match. For an _include_ filter it specifies the related fields to include.

#### Using “stringified” JSON in REST queries

Instead of the standard REST syntax described above, you can also use “stringified JSON” in REST queries. To do this, simply use the JSON specified for the Node syntax, as follows:

```
?filter={ Stringified-JSON }
```

where _Stringified-JSON_ is the stringified JSON from Node syntax. However, in the JSON all text keys/strings must be enclosed in quotes (“).

> **Important**:
>
> When using stringified JSON, you must use an equal sign after ?filter in the query string.
>
> For example: ```http://localhost:8000/users?filter={%22where%22:{%22_id%22:%225b58059184cee4d64769b750%22}}```

For example: ```GET /users?filter={"where":{"_id":"5b58059184cee4d64769b750"}}```

#### Filtering arrays of objects

The filters middleware implements each model’s filter syntax. Using this middleware, you can filter arrays of objects using the same filter syntax supported by ```MyModel.find(filter)```.

Here is a basic example using the new module.

```javascript
const data = [{n: 1}, {n: 2}, {n: 3, id: 123}];
const filter = {where: {n: {gt: 1}}, skip: 1, fields: ['n']};
const { filterDocs } = require('./middlewares/filters');
const filtered = filterDocs(data, filter);
console.log(filtered); // => [{n: 3}]
```

For a bit more detail, say you are parsing a comma-separated value (CSV) file, and you need to output all values where the price column is between 10 and 100. To use the filter syntax you would need to either create your own CSV connector or use the memory connector, both of which require some extra work not related to your actual goal.

Once you’ve parsed the CSV (with a module like ```node-csv```) you will have an array of objects like this, for example (but with, say, 10,000 unique items):

```javascript
[
    {price: 85, id: 79},
    {price: 10, id: 380},
    //...
]
```

To filter the rows you could use generic JavaScript like this:

```javascript
data.filter(function(item) {
    return item.price < 100 && item.price >= 10;
});
```

This is pretty simple for filtering, but sorting, field selection, and more advanced operations become a bit tricky. On top of that, you are usually accepting the parameters as input.

For example:

```javascript
const userInput = {min: 10, max: 100};

data.filter(function(item) {
    return item.price < userInput.min && item.price >= userInput.max;
});
```

You can rewrite this easily as a Model filter:

```javascript
filter(data, { where: { input: { gt: userInput.min, lt: userInput.max } } });
```

Or if you just adopt the filter object syntax as user input:

```javascript
filter(data, userInput);
```

But filters supports more than just excluding and including. It supports field selection (including / excluding fields), sorting, geo/distance sorting, limiting and skipping. All in a declarative syntax that is easily created from user input.

As a REST user this is a pretty powerful thing. Typically, you will have learned how to write some complex queries using the ```find()``` filter syntax; before you would need to figure out how to do the same thing in JavaScript (perhaps using a library such as underscore). Now with the filters middleware as a module, in your client application you can re-use the same exact filter object you were sending to the server to filter the database without having to interact with the server at all.

#### Fields filter

A _fields_ filter specifies properties (fields) to include or exclude from the results.

#### REST API

```
filter[fields][_propertyName_]=<true|false>&filter[fields][propertyName]=<true|false>...
```

Note that to include more than one field in REST, use multiple filters.

You can also use stringified JSON format in a REST query.

#### Node API

```
{ fields: {_propertyName_: <true|false>, _propertyName_: <true|false>, ... } }
```

Where:

- _propertyName_ is the name of the property (field) to include or exclude.
- ```<true|false>``` signifies either ```true``` or ```false``` Boolean literal. Use ```true``` to include the property or false to exclude it from results.

By default, queries return all model properties in results. However, if you specify at least one fields filter with a value of ```true```, then by default the query will include **only** those you specifically include with filters.

#### Examples

Return only ```_id```, ```make```, and ```model``` properties:

**REST**

```
?filter[fields][name]=true&filter[fields][description]=true
```

**Node API**

```
{ fields: {contestant: true, submissions: true} }
```

Returns:

```json
[
    ...
    {
        "contestant": 1,
        "submissions": [
            "5bb37a125038960027315b29",
            "5bb37a225038960027315b2b",
            "5bb37a2c5038960027315b2c",
            "5bb37a3c5038960027315b2d"
        ]
    },
    {
        "contestant": 3,
        "submissions": [
            "5bb37a4c5038960027315b2e"
        ]
    }
    ...
]
```

Exclude the ```_id``` property:

**REST**

```
?filter[fields][_id]=false
```

**Node API**

```
{ fields: {_id: false} }
```

#### Include filter

An include filter enables you to include results from related models in a query, to optimize the number of requests.

The value of the include filter can be a string, an array, or an object.

> **Important**:
>
> You can use an include filter with ```find()```, ```findById()``` and ```deleteMany()```.

#### REST API

```filter[include][relatedModel]=propertyName``` You can also use stringified JSON format in a REST query.

#### Node API

```
{include: 'relatedModel'}
{include: ['relatedModel1', 'relatedModel2', ...]}
{include: {relatedModel1: [{relatedModel2: 'relationName'} , 'relatedModel']}}
```

Where:

- _relatedModel_, _relatedModel1_, and _relatedModel2_ are the names (pluralized) of related models.
- _relationName_ is the name of a relation in the related model.

#### Examples

Include relations without filtering:

```javascript
Order.find({include: 'customer'}, function() { /* ... */ });
```

Other examples where this filter may apply, e.g:

Return all user posts and orders with two additional requests

```javascript
User.find({include: ['posts', 'orders']}, function() { /* ... */ });
```

Return all post owners (users), and all orders of each owner:

```javascript
Post.find({include: {owner: 'orders'}}, function() { /* ... */ });
```

Return all post owners (users), and all friends and orders of each owner:

```javascript
Post.find({include: {owner: ['friends', 'orders']}}, function() { /* ... */ });
```

Return all post owners (users), and all posts and orders of each owner. The posts also include images.

```javascript
Post.find({include: {owner: [{posts: 'images'} , 'orders']}}, function() { /* ... */ });
```

#### Include with filters

In some cases, you may want to apply filters to related models to be included.

> **Note**:
>
> When you apply filters to related models, the query returns results from the first model plus any results from related models with the filter query, similar to a “left join” in SQL.

The API supports that with the following syntax (for example):

```javascript
Order.find().populate({
    path: `${inclusion.prop}`,
    select: `${inclusion.props}`
});
```

#### REST examples

These examples assume an article model with an author relationship to a user model.

Return all scores including their submissions:

```
/scores?filter[include]=submissions
```

Return all scores including their submissions which also includes the contestantNumber and solutions:

```
/scores?filter[include]=submissions&filter[include][submissions]=contestantNumber&filter[include][submissions]=solutions
```

Return first two scores including their submissions which also includes the contestantNumber and solutions:

```
/scores?filter[include]=submissions&filter[include][submissions]=contestantNumber&filter[include][submissions]=solutions&filter[limit]=2
```

#### Limit filter

A _limit_ filter limits the number of records returned to the specified number (or less).

#### REST API

```
filter[limit]=_n_
```

You can also use stringified JSON format in a REST query.

#### Node API

```
{limit: _n_}
```

Where _n_ is the maximum number of results (records) to return.

#### Examples

Return only the first five query results:

**REST**

```
/users?filter[limit]=5
```

**Node API**

```javascript
Users.find({limit: 5},  function() {
    //...
})
```

#### Order filter

An _order_ filter specifies how to sort the results: ascending (ASC) or descending (DESC) based on the specified property.

#### REST API

Order by one property:

```
filter[order]=propertyName <ASC|DESC>
```

Order by two or more properties:

```
filter[order][0]=propertyName <ASC|DESC>&filter[order][1][propertyName]=<ASC|DESC>...
```

You can also use stringified JSON format in a REST query.

#### Node API

Order by one property:

```
{ order: 'propertyName <ASC|DESC>' }
```

Order by two or more properties:

```
{ order: ['propertyName <ASC|DESC>', 'propertyName <ASC|DESC>',...] }
```

Where:

- _propertyName_ is the name of the property (field) to sort by.
- ```<ASC|DESC>``` signifies either ASC for ascending order or DESC for descending order.

#### Examples

Return the three articles, sorted by the ```createdAt``` property:

**REST**

```
/scores?filter[order]=createdAt%20DESC&filter[limit]=3
```

**Node API**

```javascript
Scores.find({
    order: 'createdAt DESC',
    limit: 3
});
```

#### Skip filter

A skip filter omits the specified number of returned records. This is useful, for example, to paginate responses.

Use ```offset``` as an alias for ```skip```.

#### REST API

```
?filter=[skip]=n
```

You can also use stringified JSON format in a REST query.

#### Node

```
{skip: n}
```

Where n is the number of records to skip.

#### Examples

This REST request skips the first 50 records returned:

```
/scores?filter[skip]=50
```

The equivalent query using the Node API:

```javascript
Scores.find( {skip: 50},  function() { /* ... */ } )
```

**Pagination example**

The following REST requests illustrate how to paginate a query result. Each request request returns ten records: the first returns the first ten, the second returns the 11th through the 20th, and so on…

```
/scores?filter[limit]=10&filter[skip]=0
/scores?filter[limit]=10&filter[skip]=10
/scores?filter[limit]=10&filter[skip]=20
...
```

Using the Node API:

```javascript
Scores.find({limit: 10, skip: 0},  function() { /* ... */ });
Scores.find({limit: 10, skip: 10}, function() { /* ... */ });
Scores.find({limit: 10, skip: 20}, function() { /* ... */ });
```

#### Where filter

> A _where_ filter specifies a set of logical conditions to match, similar to a WHERE clause in a SQL query.

#### REST API

In the first form below, the condition is equivalence, that is, it tests whether _property_ equals _value_. The second form below is for all other conditions.

```
filter[where][property]=value
```

```
filter[where][property][op]=value
```

For example, if there is a cars model with an ```odo``` property, the following query finds instances where the ```odo``` is greater than 5000:

```
/cars?filter[where][odo][gt]=5000
```

For example, here is a query to find cars with ```odo``` is less than 30,000:

```
/cars?filter[where][odo][lt]=30000
```

You can also use stringified JSON format in a REST query.

#### Filter limit

> Important:
>
> There is a limit of twenty filters (combined with AND or OR) using this format, due to the use of [qs](https://github.com/ljharb/qs#parsing-arrays). When there are more than twenty, the filter is converted into an Object where it is expecting an Array.

**Encode filter object as JSON**

```
http://localhost:8000/scores?filter={"where":{"or":[{"_id":1},{"_id":2},...,{"_id":20"},{"_id":21}]}}
```

**Node API**

#### Where clause for queries

For query methods such as ```find()``` or ```findById()```, use the form below to test equivalence, that is, whether _property_ equals _value_.

```
{ where: { property: value }}
```

```javascript
Scores.find({ where: { contestant: 1 } });
```

The equivalent REST query would be:

```
/scores?filter[where][contestant]=1
```


## Additional Resources

#### Global Knowledge

- [node.js](https://nodejs.org/ "node")
- [mongodb](https://www.mongodb.com/ "mongodb")

#### Required Modules

- [bcrypt](https://www.npmjs.com/package/bcrypt "bcrypt npm")
- [bluebird](http://bluebirdjs.com/docs/api-reference.html "bluebird npm")
- [body-parser](https://www.npmjs.com/package/body-parser "body-parser npm")
- [cors](https://www.npmjs.com/package/cors "cors npm")
- [dotenv](https://www.npmjs.com/package/dotenv "dotenv npm")
- [express](https://expressjs.com/ "express npm")
- [express-promise-router](https://www.npmjs.com/package/express-promise-router "express-promise-router")
- [helmet](https://www.npmjs.com/package/helmet "helmet npm")
- [joi](https://www.npmjs.com/package/joi "joi npm")
- [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken "jsonwebtoken npm")
- [mongoose](http://mongoosejs.com/ "mongoose")
- [morgan](https://www.npmjs.com/package/morgan "morgan npm")

#### Required Modules for Testing and Development

- [mocha](https://mochajs.org/ "mocha npm")
- [chai](https://www.npmjs.com/package/chai "chai npm")
- [chai-http](http://www.chaijs.com/plugins/chai-http/ "chai-http npm")
- [nodemon](https://nodemon.io/ "nodemon")

Done and Dusted! ;)
