# LTS Image.
FROM node:8-alpine

# Create app directory inside th eimage.
WORKDIR /usr/src/app

# Install additional os packages required to make the project run.
# Related issues:
#   - node-gyp needs build tools
#   - unable to build package due python is not installed
RUN apk --no-cache add g++ gcc libgcc libstdc++ linux-headers make python && \
    npm install --quiet node-gyp -g

# Install app dependencies.
# A wildcard is used to ensure both package.json and package-lock.json are copied
# (available since npm@5+). The file package-lock.json has been excluded due to the yarn.lock
# file was included in order to work with yarn.
COPY package.json yarn.lock ./

# Install node modules.
# for production:
# RUN yarn install --prod
RUN yarn install

# To bundle your app's source code inside the Docker image.
COPY . .

# Expose the image ports for accessing through them outside the image.
EXPOSE 8000

# Run default commands.
# for production:
# CMD ["yarn", "start"]
CMD ["yarn", "dev"]
