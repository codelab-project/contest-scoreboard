Contest Scoreboard
==================

This small project has been created for testing purposes only and it's aimed to solve some basic concepts and relevant matters related to a RESTful API based on _Express.js_ and _MongoDB_.

---

## Table of Contents

* [Getting started](#getting-started)
    * [Software requirements](#software-requirements)
    * [Server setup](#server-setup)
        * [Working locally](#working-locally)
        * [Working via docker](#working-via-docker)
* [API documentation](#api-documentation)
* [License](#license)

## Getting started

### Software requirements:

#### Requirements in order to work locally

- Node.js (required: **v8.11.4** recommended or **higher**) with NPM.
- MongoDB (required: **v3.4.15** recommended or **higher**).
- Yarn (optional: however it's been used by default, if you have not installed it, just use npm instead!).

#### Requirements in order to work via docker

- Docker (required: **v18.06.1** recommended or **higher**).

### Server setup

Clone the repo from Github and set the environment variables.

```shell
git clone git@bitbucket.org:codelab-project/contest-scoreboard.git
cd contest-scoreboard

# Set the global variables used by this project via dotenv.
# You can use sample file and replace the respective values for each variable as follows:
cp env.sample .env
```

> **Heads up**!
>
> The app runs on port _8000_ by default, if you want to use a different port, you can configured it within the ```.env``` file. If you change the port, it must be set in the files Dockerfile and docker-compose.yml as well.
>
> If you use npm as your default package manager, don't forget to update the ```yarn.lock``` file.
>
> In order to perform a good update, just remove the ```node_modules``` directory, then run:
> ```yarn install``` in order to update ```yarn.lock``` file.
>
> Do exactly the same in order to update the ```package-lock.json``` file in case you use yarn as your default package manager.

### Working locally

#### Install the project dependencies

```shell
# Development
yarn install        # via npm: npm install

# Production
yarn install --prod # via npm: npm install --only=production
```

#### Start everything up

Development mode:

```shell
# Invoking the dev script.
yarn dev            # via npm: npm run dev
```

Production mode:

```shell
# Invoking the start script.
yarn start          # via npm: npm run start
```

Run tests:

```shell
# Invoking the test script.
yarn test           # via npm: npm run test
```

### Working via docker

#### Compose the docker containers

```shell
docker-compose up -d
```

Something like this will be displayed:

```
Building app
Step 1/8 : FROM node:8-alpine
 ---> 8adf3c3eb26c
Step 2/8 : WORKDIR /usr/src/app
 ---> Running in d53a818ca2d8
Removing intermediate container d53a818ca2d8
 ---> 52afbbb898bb
Step 3/8 : RUN apk --no-cache add g++ gcc libgcc libstdc++ linux-headers make python &&     npm install --quiet node-gyp -g
 ---> Running in c9a12a53eb8f
...
Creating sb-db ... done
Creating sb-app ... done
```

The -d option is for a detached mode allowing to run containers in the background, once the new containers has been created, it prints their names. Find out more at [docker's documentation site](https://docs.docker.com/v17.09/compose/reference/up/).

#### For checking the containers status just run:

```shell
docker-compose ps -a
```

Something like this will be displayed:

```
 Name              Command             State            Ports
-----------------------------------------------------------------------
sb-app   yarn dev                      Up      0.0.0.0:8000->8000/tcp
sb-db    docker-entrypoint.sh mongod   Up      0.0.0.0:27017->27017/tcp

```

#### For more details about the containers status just run:

```shell
docker ps -a
```

Something like this will be displayed:

```
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                      NAMES
a3c92ce9c4f9        server_app          "yarn dev"               2 minutes ago       Up 2 minutes        0.0.0.0:8000->8000/tcp     fs-app
6b0b78ac1cb0        mongo:4.0           "docker-entrypoint.s…"   2 minutes ago       Up 2 minutes        0.0.0.0:27017->27017/tcp   fs-db
```

#### For accessing to any container just run:

```shell
docker exec -it <CONTAINER> <SHELL>
```

#### In order to view the logs for each Docker container in real time, just run:

```shell
docker logs -f <CONTAINER>
```

The ```-f``` or ```--follow``` option will show live log output.

#### Run tests through docker:

```shell
docker exec -it <CONTAINER> yarn test
```

Something like this will be displayed:

```
yarn run v1.6.0
$ ./node_modules/mocha/bin/mocha "src/tests/**/*.js"


  User Tests
    ✓ POST /auth/signup (sign up a new user in order to perform the tests) (115ms)
    ✓ POST /users (create a user) (91ms)
    ✓ POST /users (create multiple users) (393ms)
    ✓ GET /users
    ✓ GET /users (filter[where][email]=john.doe@fsociety.com)
    ✓ GET /users (filter[fields][name]=true&filter[fields][email]=true)
    ✓ GET /users (filter[order]=name ASC)
    ✓ GET /users (filter[limit]=1)
    ✓ GET /users (filter[skip]=4)
    ✓ GET /users/:userId
    ✓ PUT /users/:userId (update the entire document - recently created user)
    ✓ PATCH /users/:userId (update some filds belonging to the recently created user)
    ✓ DELETE /users/:userId (delete the user created for testing purposes "John Doe")
    ✓ DELETE /users (filter[include]=avatar&filter[include][avatar]=randomuser)
    ✓ DELETE /users (filter[include]=avatar&filter[include][avatar]=discordemoji)


  15 passing (848ms)

Done in 1.68s.
```

---

## API Documentation

Check out the [API Documentation](server/docs/API.md "API Documentation").

---

## License

This package is available under the [MIT license](LICENSE "MIT license").